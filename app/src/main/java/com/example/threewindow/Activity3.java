package com.example.threewindow;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class Activity3 extends AppCompatActivity {

    private Button button;
    private Button button2;
    private TableLayout table = findViewById(R.id.table);
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity1();
            }
        });

        button2 = (Button) findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity2();
            }
        });
        TableIn();
    }
    void TableIn(){
        TextView textView1 = findViewById(R.id.textView3);
        textView1.setText("Междуреченск ");
        TextView textView2 = findViewById(R.id.textView2);
        textView2.setText("-28 ");
        TextView textView3 = findViewById(R.id.textView5);
        textView3.setText("Новосибирск ");
        TextView textView4 = findViewById(R.id.textView4);
        textView4.setText("-25 ");
        TextView textView5 = findViewById(R.id.textView7);
        textView5.setText("Санкт-Петербург ");
        TextView textView6 = findViewById(R.id.textView6);
        textView6.setText("-15 ");
        TextView textView7 = findViewById(R.id.textView9);
        textView7.setText("Москва ");
        TextView textView8 = findViewById(R.id.textView8);
        textView8.setText("-8 ");
    }
    void openActivity1() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    void openActivity2() {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }
}